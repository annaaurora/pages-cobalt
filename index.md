---
title: papojari's website
layout: default.liquid
---
Hello,

I am papojari.

I design websites, develop games and write packages and my system configuration in Nix. Occasionally I'll create something in Blender, a 3d software. I really dig Linux stuff. My desktop operating system is [NixOS](https://nixos.org/). As you can read, I heavily use open source software. Yes I say *open source* because saying *free* (as in freedom) leaves out *copyleft* software. Commiting to a git repository is very satisfying btw.

You will find my a blog, some other posts and a link tree on this website. I also put my [first game](static/find-billy/find-billy.html) on this site. (It **sometimes** doesn't work when hosted on [Codeberg Pages](https://codeberg.page) probably because it only allows for static sites)
