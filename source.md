---
title: Source code
layout: default.liquid
---
The source code for this website is available at [https://codeberg.org/papojari/pages-cobalt](https://codeberg.org/papojari/pages-cobalt) with the generated site available at [https://codeberg.org/papojari/pages](https://codeberg.org/papojari/pages).
