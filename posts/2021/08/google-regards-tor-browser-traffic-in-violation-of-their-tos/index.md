---
title: Google regards Tor Browser traffic in violation of their ToS
published_date: "2021-08-21 14:09:59 +0000"
layout: default.liquid
is_draft: false
---
For privacy reasons I occasionally use *Tor Browser* or the *Tor* feature in [*Brave*](https://brave.com/). When connecting to *Google* sites you face this:

> <img class="dark-image" src="google-unusual-traffic.webp">
>
> [https://www.google.com/sorry/index?continue=https://google.com/#](https://www.google.com/sorry/index?continue=https://google.com/#)

I understand that Google being mostly an advertising company wants to suck on all of my data but the

> In violation of the Terms of Service
>
> [https://www.google.com/sorry/index?continue=https://google.com/#](https://www.google.com/sorry/index?continue=https://google.com/#)

is kind of odd. After quickly flying over their ToS, there isn't really anything specifically adressed at this problem I'm not going to bother reading their whole Terms of Service so I'm going to make some assumptions here. Are they saying that not providing enough data for Google to make money off *(like Tor Browser)* is in violation of their Terms of Service? I'm kind of digusted by that. As far as I know Tor Browser doesn't send many request very quickly, like they say.

> Very dumb, but it is Google, so what do you expect?
>
> [@bowuigi---now-more-based:kde.org](https://matrix.to/#/@bowuigi---now-more-based:kde.org) on matrix