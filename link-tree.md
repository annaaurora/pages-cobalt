---
title: Link tree
layout: default.liquid
tags: [ git repository keyoxide codeberg gitlab github]
---
## Identity verification

<a href="https://keyoxide.org/319A982F5F12013B730139FF5D98BEEC20C9695C">
	<p class="badge">
		<img class="badge-icon" src="https://keyoxide.org/static/img/logo_circle.png" alt="Keyoxide icon" height="25"/>papojari
	</p>
</a>

## git hosting sites accounts

<a href="https://codeberg.org/papojari">
	<p class="badge">
		<img class="badge-icon" src="/static/logos/codeberg.svg" alt="Codeberg icon" height="25"/>papojari
	</p>
</a>

<a href="https://github.com/papojari">
	<p class="badge">
		<img class="badge-icon dark-image" src="/static/fontawesome/svgs/brands/github.svg" alt="GitHub icon" height="25"/>papojari
	</p>
</a>

<a href="https://gitlab.com/papojari">
	<p class="badge">
		<img class="badge-icon orange-icon" src="/static/fontawesome/svgs/brands/gitlab.svg" alt="GitLab icon" height="25"/>papojari
	</p>
</a>

## Social media

<a href="https://reddit.artemislena.eu/u/veggushroom">
	<p class="badge">
		<img class="badge-icon red-orange-icon" src="/static/fontawesome/svgs/brands/reddit-alien.svg" alt="Reddit icon" height="25"/>veggushroom
	</p>
</a>

<h2 id="contact">Contact</h2>

### Report issues with this website

<a href="https://codeberg.org/papojari/pages-cobalt/issues">
	<p class="badge">
		<img class="badge-icon" src="/static/logos/codeberg.svg" alt="Codeberg icon" height="25"/>papojari/pages-cobalt/issues
	</p>
</a>

### matrix

The best way to contact me is [matrix](https://matrix.org/). Your messages will only be able to be read by me.

<a href="https://matrix.to/#/@papojari:artemislena.eu">
	<p class="badge">
		<img class="badge-icon dark-image" src="/static/logos/matrix.svg" alt="Matrix icon" height="25"/>@papojari:artemislena.eu
	</p>
</a>

> If you are new, do not register an account on the matrix.org server, instead, have a look at [this list.](https://github.com/techlore/faq-bot/blob/6c257e35c9033de7222be16528f3ab39a466b56a/faq.json#L10)
>
> [https://artemislena.eu/contact.html](https://artemislena.eu/contact.html)

### Jami

> [Jami](https://jami.net/) is an encrypted peer to peer messenger. It can offer better privacy and possibly be easier to use than Matrix, for direct chats.
>
> [https://artemislena.eu/contact.html](https://artemislena.eu/contact.html)


|   username |                                 identifier |
| ---------: | -----------------------------------------: |
| `papojari` | `8114b1b5d3dc1343880206515d8d69e96fb4ef7d` |

<img class="qr-code" src="/static/jami-qr-code.svg" alt="Jami contact qr code"></img>

You can also scan the above qr code in the Jami app instead of entering my username or identifier.

### E-mail

If you really don't want to use [matrix](https://matrix.org/) or [Jami](https://jami.net/), I can be reached via E-mail at `papojari` *DASH* `git` *DOT* `ovoid` *AT* `aleeas` *DOT* `com`

Our emails will be unencrypted meaning anyone that an email passes will be able to read it. In this case it would be my email provider and yours respectively. Maybe I will add a public key later for encryption.

## git repos

- This website
  - [Build output](https://codeberg.org/papojari/pages)
  - [Build input](https://codeberg.org/papojari/pages-cobalt)
- [NixOS configuration](https://codeberg.org/papojari/nixos-config)
- [Arch Linux configuration *(outdated)*](https://codeberg.org/papojari/archlinux-config-desktop)
- [find-billy](https://codeberg.org/papojari/find-billy)

## Websites

- [artemislena.eu](https://artemislena.eu)
- [based.cooking](https://based.cooking)

## Operating systems

- [NixOS](https://nixos.org/)
- [Arch Linux](https://archlinux.org/)

## Terminal emulators

- [alacritty](https://github.com/alacritty/alacritty)

## Xorg window managers/wayland compositors

- [sway](https://github.com/swaywm/sway)
- [bspwm](https://github.com/baskerville/bspwm)
- [dwm](https://dwm.suckless.org/)
