#!/bin/sh
pushd /git-repos/pages-cobalt

printf %s "Commit message: "
read COMMIT_MESSAGE

git add .
git commit -m "$COMMIT_MESSAGE" &&
git push

cd _site

git add .
git commit -m "$COMMIT_MESSAGE" &&
git push

popd