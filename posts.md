---
title: "papojari's Blog"
layout: posts.liquid
---
{%for post in collections.posts.pages limit:10%}
<article lang="{{post.data.lang}}">

### [{{post.title}}]({{post.permalink}})
{{post.excerpt}}<a href="{{post.permalink}}">Read more…</a>
</article>
{%endfor%}
