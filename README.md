<div align="center">

<h1>papojari's website's build input repository</h1>

<p>
	<a href="https://forthebadge.com"><img src="https://forthebadge.com/images/badges/uses-html.svg">
	<a href="https://forthebadge.com"><img src="https://forthebadge.com/images/badges/uses-css.svg">
	<a href="https://forthebadge.com"><img src="https://forthebadge.com/images/badges/check-it-out.svg">
</p>

---

[Build output repository](https://codeberg.org/papojari/pages)

[Rendered website](https://papojari.codeberg.page)

[Licenses](https://papojari.codeberg.page/licenses.html)

[Cobalt static site generator](https://cobalt-org.github.io)

</div>