---
title: Art portfolio
layout: default.liquid
---
## 2021

# Hexagonal prism wallpaper

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2021/08/29 | Blender  | Cycles        |

![lossy image](Hexagonal-prism-wallpaper/lossy.webp)

[lossless image](Hexagonal-prism-wallpaper/lossless.webp)

[blender file](Hexagonal-prism-wallpaper/default.blend.zst)

# RG(host of le )B(edroom)

| Date       | Software | Render engine | PBR materials                       |
| ---------- | -------- | ------------- | ----------------------------------- |
| 2021/08/08 | Blender  | Cycles        | [ambientCG](https://ambientcg.com/) |

![lossy image](RGhost_of_le_Bedroom/lossy.webp)

[lossless image](RGhost_of_le_Bedroom/lossless.webp)

Without the PBR materials from ambientCG this would not look half as good. They're all licensed under CC0. With this piece I combined a bunch of stuff like modelling a PC and a whole bunch of other things with softbody and cloth simulations to make a nice room. I improved my compositing skills quite a bit too. All the work took several days though the time was probably greatly reduced by my *new* hardware like a better processor and graphics card.

---

### NixOS 3d showcase

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2021/06/05 | Blender  | Cycles        |

![lossy image](NixOS-3d-showcase/lossy.webp)

[lossless image](NixOS-3d-showcase/lossless.webp)

I created this piece as a background for my desktop. It tries to showcase what [NixOS](https://nixos.org) is about.

---

### Rover

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2021/04/24 | Blender  | Cycles        |

![lossy image](Rover/lossy.webp)

[lossless image](Rover/lossless.webp)

This rover is supposed to be self-sufficient. It captures energy through it's solar panels communicates with it's satellite dish and takes footage with it's too cameras

---

### Honda Sports EV

| Date       | Software | Render engine | PBR materials                                            |
| ---------- | -------- | ------------- | -------------------------------------------------------- |
| 2021/02/09 | Blender  | Cycles        | Walls and floor from [ambientCG](https://ambientcg.com/) |

![lossy image](Honda-Sports-EV/lossy.webp)

[lossless image](Honda-Sports-EV/lossless.webp)

I modelled this car off the Honda Sports EV Concept from 2018.

---

### Viking

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2021/02/09 | Blender  | Cycles        |

![lossy image](Viking/lossy.webp)

[lossy image](Viking/lossless.webp)

---

### AK47

| Date       | Software | Render engine | PBR materials                                                     |
| ---------- | -------- | ------------- | ----------------------------------------------------------------- |
| 2021/01/03 | Blender  | Eevee         | Concrete, metal and wood from [ambientCG](https://ambientcg.com/) |

![lossy image](AK47/lossy.webp)

[lossy image](AK47/lossless.webp)

---

## 2020

### Totebot

| Date       | Software |
| ---------- | -------- |
| 2020/05/22 | Krita    |

![lossy image](Totebot/lossy.webp)

[lossy image](Totebot/lossless.webp)

This is a Fanart of mine from the game *[Scrap mechanic](https://www.scrapmechanic.com/)*.

---

### Obama Prism

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2020/05/22 | Blender  | Cycles        |

![lossy image](Obama-Prism/lossy.webp)

[lossy image](Obama-Prism/lossless.webp)

### City block

| Date       | Software | Render engine |
| ---------- | -------- | ------------- |
| 2020/05/22 | Blender  | Eevee         |

![lossy image](City-block/lossy.webp)

[lossy image](City-block/lossless.webp)

This was modelled with inspiration from [Imphenzia](https://www.youtube.com/watch?v=ewKwYsQjBdQ).